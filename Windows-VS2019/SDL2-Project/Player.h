
#ifndef PLAYER_H_
#define PLAYER_H_
#include "SDL2Common.h"
#include "Animation.h"
typedef struct Player
{
    // Texture which stores the sprite sheet (this
    // will be optimised).
    SDL_Texture* playerTexture = nullptr;

    // Player properties
    SDL_Rect targetRectangle;

    float playerSpeed = 50.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;
    float xVelocity = 0.0f;
    float yVelocity = 0.0f;

    // Sprite information
    const int KING_SPRITE_HEIGHT = 64;
    const int KING_SPRITE_WIDTH = 32;

}; Player;
void initPlayer(Player* player, SDL_Renderer* renderer);
void destPlayer(Player* player);
void drawPlayer(Player* player, SDL_Renderer* renderer);
void processInput(Player* player, const Uint8* keyStates);
void updatePlayer(Player* player, float timeDeltaInSeconds);



#endif