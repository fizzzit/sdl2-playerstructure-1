#include "Player.h"
#include "TextureUtils.h"


/**
 * initPlayer
 *
 * Function to populate an animation structure from given paramters.
 *
 * @param player Player structure to populate
 * @param renderer Target SDL_Renderer to use for optimisation.
 * @return void?  Could do with returning a value for success/error
 */
void initPlayer(Player* player, SDL_Renderer* renderer)
{

    // Create player texture from file, optimised for renderer 
    // There are better ways of doing this, i.e. a Resource Manager 
    // This will do for now :-D
    player->playerTexture = createTextureFromFile("assets/images/undeadking.png", renderer);

    // Allocate memory for the animation structure
    player->walkLeft = new Animation;

    // Setup the animation structure
    initAnimation(player->walkLeft, 3, player->KING_SPRITE_WIDTH, player->KING_SPRITE_HEIGHT, -1, 2);

    // Target is the same size of the source
    // We could choose any size - experiment with this :-D
    player->targetRectangle.w = player->KING_SPRITE_WIDTH;
    player->targetRectangle.h = player->KING_SPRITE_HEIGHT;
}