/*
  Starting to use functions to avoid repeating code and manage complexity. 
*/

//For exit()
#include <stdlib.h>
// For round()
#include <math.h>
#include "SDL2Common.h"
#include "Animation.h"
#include "Player.h"
#include "TextureUtils.h"
// The rest of the main.cp

#if defined(_WIN32) || defined(_WIN64)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

/*****************************
 * Global Variables           *
 * ***************************/

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;


/*****************************
 * Structures                *
 * **************************/

typedef struct Animation
{
    int maxFrames;
    int currentFrame;

    float frameTimeMax;
    float accumulator;

    SDL_Rect *frames;
};


Animation* walkLeft;


/****************************
 * Function Prototypes      *
 * **************************/

SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer);

void initAnimation(Animation* anim, 
                   int noOfFrames, 
                   const int SPRITE_WIDTH, const int SPRITE_HEIGHT, 
                   int row, int col);
void destAnimation(Animation* anim);


int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    
    //Declare Player
    Player player;

    // Window control 
    SDL_Event event;
    bool quit = false;  //false
    
    // Keyboard
    const Uint8 *keyStates;
    
    // Timing variables
    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;



    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
     // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);
      
    /**********************************
     *    Setup background image     *
     * ********************************/

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);


    /**********************************
     * Setup Player
     * ********************************/

    initPlayer(&player,gameRenderer);

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();


     // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist - https://gafferongames.com/post/fix_your_timestep/

        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Handle input 

        if( SDL_PollEvent( &event ))  // test for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        // Process Inputs
        processInput(&player, keyStates);

        // Update Game Objects

        updatePlayer(&player, timeDeltaInSeconds);

        //Draw stuff here.

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

        drawPlayer(&player, gameRenderer);

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);

    }
    
    //Clean up!
    destPlayer(&player);  
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    exit(0);
}


/**
 * Create Texture from File
 * 
 * Function to load a texture from a given filename and render context.
 * 
 * @param filename Filename of the file to load. 
 * @param renderer A pointer to the current SDL_Renderer. 
 * @return A pointer to the SDL_Texture created, or a nullptr on error. 
 */ 
SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer)
{
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     texture = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Load the sprite to our temp surface
    temp = IMG_Load(filename);

    if(temp == nullptr)
    {
        printf("%s image not found!", filename);
    }    
    else  // Improved error handling, if we have no surface we can't make a texture.
    {
        // Create a texture object from the loaded image
        // - we need the renderer we're going to use to draw this as well!
        // - this provides information about the target format to aid optimisation.
        texture = SDL_CreateTextureFromSurface(renderer, temp);

        // Clean-up - we're done with 'image' now our texture has been created
        SDL_FreeSurface(temp);
        temp = nullptr;
    }

    return texture;
}


/**
 * initAnimation
 * 
 * Function to populate an animation structure from given paramters. 
 * 
 * @param anim Animation structure to populate 
 * @param noOfFrames Frames of animation
 * @param SPRITE_WIDTH Width of the sprite
 * @param SPRITE_HEIGHT Height of the sprite
 * @param row Row of the grid to take the sprites from or -1 if working along a row. 
 * @param col Column of the grid to take the sprites from or -1 if along a column.
 * @return void?  Could do with returning a value for success/error 
 */
void initAnimation(Animation* anim, 
                   int noOfFrames, 
                   const int SPRITE_WIDTH, const int SPRITE_HEIGHT, 
                   int row, int col)
{

    // set frame count.
    anim->maxFrames = noOfFrames;

    // allocate frame array
    anim->frames = new SDL_Rect[anim->maxFrames]; 

     //Setup animation frames - fixed row!
    for(int i = 0; i < anim->maxFrames; i++)
    {
        if(row == -1)
        {
            anim->frames[i].x = (i * SPRITE_WIDTH); //ith col.
            anim->frames[i].y = (col* SPRITE_HEIGHT); //col row. 
        }
        else 
        {
            if(col == -1)
            {
                anim->frames[i].x = (row * SPRITE_WIDTH); //ith col.
                anim->frames[i].y = (i * SPRITE_HEIGHT); //col row. 
            }
            else
            {
                printf("Bad paramters to initAnimation!\n");
                //return an error?
            }
            
        }
        
        anim->frames[i].w = SPRITE_WIDTH;
        anim->frames[i].h = SPRITE_HEIGHT;
    }

    //set current animation frame to first frame. 
    anim->currentFrame = 0;

    //zero frame time accumulator
    anim->accumulator = 0.0f;
}

/**
 * destAnimation
 * 
 * Function to clean up an animation structure.  
 * 
 * @param anim Animation structure to destroy
 */
void destAnimation(Animation* anim)
{
    //Free the memory - we allocated it with new
    // so must use the matching delete operation.

    delete anim->frames;

    // Good practice to set pointers to null after deleting
    // ths prevents accidental access. 
    anim->frames = nullptr;
}



/**
 * destPlayer
 * 
 * Function to clean up an player structure.  
 * 
 * @param player Player structure to destroy
 */
void destPlayer(Player* player)
{
    // Clean up animations - free memory
    destAnimation(player->walkLeft);

    // Clean up the animaton structure
    // allocated with new so use delete. 
    delete player->walkLeft;
    player->walkLeft = nullptr;

    // Clean up 
    SDL_DestroyTexture(player->playerTexture);
    player->playerTexture = nullptr; 
}

/**
 * drawPlayer
 * 
 * Function draw a Player structure
 * 
 * @param player Player structure tp draw
 * @param renderer SDL_Renderer to draw to
 */
void drawPlayer(Player* player, SDL_Renderer *renderer)
{
    // Get current animation (only have one)!
    Animation* current = player->walkLeft;

    SDL_RenderCopy(renderer, player->playerTexture, &current->frames[current->currentFrame], &player->targetRectangle);
}

/**
 * processInput
 * 
 * Function to process inputs for the player structure
 * Note: Need to think about other forms of input!
 * 
 * @param player Player structure processing the input
 * @param keyStates The keystates array. 
 */
void processInput(Player *player, const Uint8 *keyStates)
{
    // Process Player Input
        
    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 

    // This could be more complex, e.g. increasing the vertical 
    // input while the key is held down. 
    if (keyStates[SDL_SCANCODE_UP]) 
    {
        verticalInput = -1.0f;
    }
    else 
    {
        verticalInput = 0.0f;
    }

    // Calculate player velocity. 
    // Note: This is imperfect, no account taken of diagonal!
    player->xVelocity = verticalInput * player->playerSpeed;
    player->yVelocity = horizontalInput * player->playerSpeed;
}

/**
 * updatePlayer
 * 
 * Function to update the player structure and its components
 * 
 * @param player Player structure being updated
 * @param timeDeltaInSeconds the time delta in seconds
 */
void updatePlayer(Player* player, float timeDeltaInSeconds)
{
    // Calculate distance travelled since last update
    float yMovement = timeDeltaInSeconds * player->xVelocity;
    float xMovement = timeDeltaInSeconds * player->yVelocity;

    // Update player position.
    player->playerX += xMovement;
    player->playerY += yMovement;

    // Move sprite to nearest pixel location.
    player->targetRectangle.y = round(player->playerY);
    player->targetRectangle.x = round(player->playerX);

    // Store animation time delta
    player->walkLeft->accumulator += timeDeltaInSeconds;

    // Check if animation needs update
    if(player->walkLeft->accumulator > 0.4f) //25fps?
    {
        player->walkLeft->currentFrame++;
        player->walkLeft->accumulator = 0.0f;

        if(player->walkLeft->currentFrame >= player->walkLeft->maxFrames)
        {
            player->walkLeft->currentFrame = 0;
        }
    }
}