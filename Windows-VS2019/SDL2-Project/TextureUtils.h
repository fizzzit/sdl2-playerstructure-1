#ifndef TEXTURE_UTILS_H_
#define TEXTURE_UTILS_H_
#include "SDL2Common.h"
SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer);
{
	SDL_Texture* texture = nullptr;
SDL_Surface* temp = nullptr;
temp = IMG_Load(filename); 
if (temp == nullptr) { printf("%s image not found!", filename);
} else { texture = SDL_CreateTextureFromSurface(renderer, temp);
SDL_FreeSurface(temp);
temp = nullptr;
}return texture; 

}
#endif
