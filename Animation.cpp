#include "Animation.h"
#include "SDL2Common.h"

void initAnimation(Animation* anim,
    int noOfFrames,
    const int SPRITE_WIDTH, const int SPRITE_HEIGHT,
    int row, int col)
{

    // set frame count.
    anim->maxFrames = noOfFrames;

    // allocate frame array
    anim->frames = new SDL_Rect[anim->maxFrames];

    //Setup animation frames - fixed row!
    for (int i = 0; i < anim->maxFrames; i++)
    {
        if (row == -1)
        {
            anim->frames[i].x = (i * SPRITE_WIDTH); //ith col.
            anim->frames[i].y = (col * SPRITE_HEIGHT); //col row. 
        }
        else
        {
            if (col == -1)
            {
                anim->frames[i].x = (row * SPRITE_WIDTH); //ith col.
                anim->frames[i].y = (i * SPRITE_HEIGHT); //col row. 
            }
            else
            {
                printf("Bad paramters to initAnimation!\n");
                //return an error?
            }

        }