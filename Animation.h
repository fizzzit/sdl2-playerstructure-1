#include "SDL2Common.h"




#ifndef ANIMATION_H_
#define ANIMATION_H_



typedef struct Animation
{
    int maxFrames;
    int currentFrame;

    float frameTimeMax;
    float accumulator;

    SDL_Rect* frames;

} Animation;
void initAnimation(Animation* anim,
	int noOfFrames,
	const int SPRITE_WIDTH,
	const int SPRITE_HEIGHT,
	int row, int col);
void destAnimation(Animation* anim);

#endif
